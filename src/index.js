import React from "react";
import ReactDOM from "react-dom/client";

import store from "./ulti/store";
import { Provider } from "react-redux";

import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home from "./page/home";
import Cart from "./page/cart";

import i18n from "./translation/i18n";
import { I18nextProvider } from "react-i18next";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <>
    <I18nextProvider i18n={i18n}>
      <Provider store={store}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/home" element={<Home />} />
            <Route path="/cart" element={<Cart />} />
          </Routes>
        </BrowserRouter>
      </Provider>
    </I18nextProvider>

    <style jsx="true">
      {`
        .pointer {
          cursor: pointer;
        }
      `}
    </style>
  </>
);
