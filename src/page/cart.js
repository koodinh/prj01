import React, { useEffect } from "react";

import { useSelector, useDispatch } from "react-redux";
import { getDetailProduct } from "../action/product";
import Modal from "../components/Modal";

import { Link } from "react-router-dom";

const Cart = () => {
  const dispatch = useDispatch();

  const { cart } = useSelector((state) => state);
  const { filter } = useSelector((state) => state);
  const { detail } = useSelector((state) => state.product);

  useEffect(() => {
    dispatch(getDetailProduct(null));
  }, []);

  return (
    <>
      <h1>Cart</h1>

      <Link to="/">Quay lại</Link>

      {cart.data.map((item) => (
        <div
          key={`cart-${item.id}`}
          onClick={() => dispatch(getDetailProduct(item))}
        >
          <p>{item.name}</p>
          <p>
            extra:{" "}
            {item.extraSelect.map((elm) => (
              <p>
                {item.extra_products.map((lab) => lab.id == elm && lab.name)}
              </p>
            ))}
          </p>

          <p>
            filter:{" "}
            {Object.keys(item.filterSelect).map((key) =>
              filter.data.map(
                (elm) =>
                  elm.id == item.filterSelect[key] && `${key}: ${elm.name}`
              )
            )}
          </p>

          <p>Số lượng: {item.quantity}</p>
          <hr />
        </div>
      ))}

      {detail.data != null && <Modal edit={true} />}
    </>
  );
};

export default Cart;
