import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import Categories from "../components/Categories";
import ProductItem from "../components/ProductItem";

import Modal from "../components/Modal";

import request from "../ulti/request";

import { getListCategoriesSuccess } from "../action/categories";

import { useTranslation } from "react-i18next";

import { Link } from "react-router-dom";

const Home = () => {
  const { t, i18n } = useTranslation();

  const dispatch = useDispatch();

  const { categories, product, filter, cart } = useSelector((state) => state);

  const changeLang = (lang) => {
    i18n.changeLanguage(lang);
  };

  useEffect(() => {
    const resCat = request("/api/categories/get", {
      params: { id_website: 4 },
    });
    resCat.then((result) => {
      dispatch(getListCategoriesSuccess(result.data.data));
    });
  }, []);

  // check dữ liệu ================================
  useEffect(() => {
    console.log("check Store Cat:", categories);
    console.log("check Store Product", product);
    console.log("check Store Filter", filter);
    console.log("check Store Cart", cart);
  }, [product, categories, cart]);

  return (
    <div>
      <select onChange={(e) => changeLang(e.target.value)}>
        <option value="vi">Tiếng Việt</option>
        <option value="en">Tiếng Anh</option>
      </select>

      <Link to="/cart">Giỏ hàng</Link>

      <div className="wrapper-cat">
        <h3>{t("content.cat")}</h3>
        <p>{t("content.cat-des")}</p>

        {categories.data.map((item) => (
          <Categories key={`cat-${item.id}`} data={item} />
        ))}
      </div>

      <br />
      <br />

      <h3>{t("content.menu")}</h3>
      <p>{t("content.menu-des")}</p>

      {product.list.data.map(
        (item) =>
          item.id_parent == 0 && (
            <ProductItem key={`prod-${item.id}`} data={item} />
          )
      )}

      {product.detail?.data != null ? <Modal /> : null}
    </div>
  );
};

export default Home;
