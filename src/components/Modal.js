import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getDetailProduct, getExtraSuccess } from "../action/product";

import request from "../ulti/request";

import { getListFilterSuccess } from "../action/filter";
import { saveCart } from "../action/cart";

const Modal = ({ edit = false }) => {
  const dispatch = useDispatch();

  const { detail } = useSelector((state) => state.product);
  const { cart } = useSelector((state) => state);

  const { filter } = useSelector((state) => state);

  const [filterSelect, setFilterSelect] = useState({});

  const [quantity, setQuantity] = useState(1);

  const [extraSelect, setExtraSelect] = useState([]);

  const [totalPrice, setTotalPrice] = useState(0);

  const addCartHandle = () => {
    let checkExid = cart.data.filter((item) => {
      if (item.id == detail.data.id) {
        return 1;
      }
    });

    if (checkExid.length == 0) {
      dispatch(
        saveCart({
          data: [
            ...cart.data,
            {
              ...detail.data,
              total: totalPrice,
              extraSelect: extraSelect,
              filterSelect: filterSelect,
              quantity: quantity,
            },
          ],
        })
      );

      return;
    }

    let newCart = cart.data.map((item) => {
      if (item.id == detail.data.id) {
        return {
          ...item,
          quantity: edit ? quantity : Number(item.quantity) + Number(quantity),
          total: totalPrice,
          extraSelect: extraSelect,
          filterSelect: filterSelect,
        };
      }
    });

    dispatch(saveCart({ data: newCart }));
  };

  useEffect(() => {
    console.log(detail);

    if (detail?.data?.id) {
      const res = request("/api/filters/get", {
        params: {
          active: 1,
          id_cat: detail.data.id_cat,
          page: 1,
          offset: 100,
          id_website: 4,
        },
      });
      res.then((result) => {
        console.log("check child filter: ", result);
        dispatch(getListFilterSuccess(result.data.data));
      });

      detail?.data?.filterSelect && setFilterSelect(detail?.data?.filterSelect);
      detail?.data?.extraSelect && setExtraSelect(detail?.data?.extraSelect);
      detail?.data?.quantity && setQuantity(detail?.data?.quantity);
    }
  }, [detail?.data?.id]);

  useEffect(() => {
    console.log("calc");

    let total = 0;

    total = detail.data.price + total;

    console.log(extraSelect);

    extraSelect.filter((item, index) => {
      detail.data.extra_products.filter((lab) => {
        if (lab.id == item) {
          total = total + lab.price;
        }
      });
    });

    Object.values(filterSelect).map((val) => {
      total = total + detail.data.filter_val[val];
    });

    setTotalPrice(total);
  }, [filterSelect, extraSelect]);

  return (
    <>
      <div className="modal pointer">
        <div className="main-modal">
          <button onClick={() => dispatch(getDetailProduct(null))}>
            Close
          </button>
          <h3>{detail.data?.name}</h3>

          <div className="filter">
            {(detail?.data?.filters || []).map((item) => (
              <div key={`filter-${item.id}`}>
                <p>{item.name}</p>

                <hr />

                {filter.data.map(
                  (lab) =>
                    lab.id_parent == item.id && (
                      <div key={`filter-${lab.id}`} style={{ display: "flex" }}>
                        <input
                          type="radio"
                          checked={
                            filterSelect[item.name] == lab.id ? true : false
                          }
                          value={lab.id}
                          onChange={(e) =>
                            setFilterSelect({
                              ...filterSelect,
                              [item.name]: e.target.value,
                            })
                          }
                        />
                        {lab.name}
                        <p
                          style={{ flex: "1", margin: "0", textAlign: "right" }}
                        >
                          {detail.data.filter_val[lab.id]} vnđ
                        </p>
                      </div>
                    )
                )}
              </div>
            ))}
          </div>

          <div className="extra">
            <h3>Ăn kèm</h3>

            {detail.data.extra_products.map((item) => (
              <div key={`extra-${item.id}`} style={{ display: "flex" }}>
                <input
                  type="checkbox"
                  value={item.id}
                  checked={extraSelect.includes(item.id)}
                  onChange={() => {
                    if (extraSelect.includes(item.id)) {
                      console.log("a");

                      let newExtraSelect = extraSelect.filter((lab) => {
                        if (lab != item.id) {
                          return lab;
                        }
                      });

                      setExtraSelect(newExtraSelect);
                    } else {
                      setExtraSelect([...extraSelect, item.id]);
                    }
                  }}
                />
                {item.name}

                <p style={{ flex: "1", margin: "0", textAlign: "right" }}>
                  {item.price} vnđ
                </p>
              </div>
            ))}
          </div>

          <div className="total">
            <h3>{totalPrice}</h3>
          </div>

          <input
            type="number"
            value={quantity}
            onChange={(e) => setQuantity(e.target.value)}
          />

          <button className="add-cart" onClick={() => addCartHandle()}>
            Thêm giỏ hàng
          </button>
        </div>
      </div>

      <style jsx="true">{`
        .modal {
          display: flex;
          background: rgba(0, 0, 0, 0.5);
          padding: 30px;
          position: fixed;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          align-items: center;
          justify-content: center;
        }
        .main-modal {
          z-index: 10;
          position: absolute;
          min-height: 50%;
          max-height: 80%;
          width: 60%;
          background: #fff;
          padding: 30px;
        }
      `}</style>
    </>
  );
};

export default Modal;
