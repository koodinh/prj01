import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

import request from "../ulti/request";

import { getListProductSuccess } from "../action/product";

const Categories = ({ data }) => {
  const dispatch = useDispatch();

  const getProduct = (id) => {
    const res = request("/api/products/get", {
      params: { active: 1, id_parent: 0, id_website: 4, id_cat: id },
    });

    res.then((result) => {
      dispatch(getListProductSuccess(result.data.data));
    });
  };

  useEffect(() => {}, []);

  return (
    <>
      <div className="item-cat pointer" onClick={() => getProduct(data?.id)}>
        <h3>{data?.name}</h3>
      </div>

      <style jsx="true">{`
        .item-cat {
          display: inline-block;
          width: 45%;
          border: 1px solid #212121;
          text-align: center;
          padding: 30px 0px;
          margin: 10px 1.5%;
        }
      `}</style>
    </>
  );
};

export default Categories;
