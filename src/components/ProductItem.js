import React from "react";
import { useDispatch } from "react-redux";

import { getDetailProduct } from "../action/product";

const ProductItem = ({ data }) => {
  const dispatch = useDispatch();

  return (
    <>
      <div
        className="item-prod pointer"
        onClick={() => dispatch(getDetailProduct(data))}
      >
        <h3>{data?.name}</h3>
      </div>

      <style jsx="true">{`
        .item-prod {
          display: inline-block;
          width: 25%;
          border: 1px solid #212121;
          background: lightgreen;
          text-align: center;
          padding: 30px 0px;
          margin: 10px 15px;
        }
      `}</style>
    </>
  );
};

export default ProductItem;
