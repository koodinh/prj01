import { combineReducers } from "redux";

import productReducer from "./productReducer";
import tableReducer from "./tableReducer";
import categoriesReducer from "./categoriesReducer";
import filterReducer from "./filterReducer";
import cartReducer from "./cartReducer";

const rootReducer = combineReducers({
  product: productReducer,
  table: tableReducer,
  categories: categoriesReducer,
  filter: filterReducer,
  cart: cartReducer,
});

export default rootReducer;
