import * as TYPE from "../contans/product";

const initialState = {
  list: {
    loading: false,
    data: [],
    mess: null,
  },
  detail: {
    loading: false,
    data: null,
    mess: null,
  },
  extra: {
    loading: false,
    data: null,
    mess: null,
  },
};

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPE.GET_LIST_PRODUCT:
      return {
        ...state,
        list: {
          ...state.list,
          loading: true,
        },
      };

    case TYPE.GET_LIST_PRODUCT_SUCCESS:
      return {
        ...state,
        list: {
          ...state.list,
          loading: false,
          data: action.data,
        },
      };

    case TYPE.GET_LIST_PRODUCT_ERROR:
      return {
        ...state,
        list: {
          ...state.list,
          loading: false,
          mess: action.mess,
        },
      };

    case TYPE.GET_EXTRA:
      return {
        ...state,
        extra: {
          ...state.extra,
          loading: true,
        },
      };

    case TYPE.GET_EXTRA_SUCCESS:
      return {
        ...state,
        extra: {
          ...state.extra,
          loading: false,
          data: action.data,
        },
      };

    case TYPE.GET_EXTRA_ERROR:
      return {
        ...state,
        extra: {
          ...state.extra,
          loading: false,
          mess: action.mess,
        },
      };

    case TYPE.GET_DETAIL_PRODUCT:
      return {
        ...state,
        detail: {
          ...state.detail,
          loading: false,
          data: action.data,
        },
      };

    default:
      return state;
  }
};

export default productReducer;
