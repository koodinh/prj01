import * as TYPE from "../contans/table";

const initialState = {
  loading: false,
  data: [],
  mess: null,
};

const tableReducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPE.GET_LIST_TABLE:
      return {
        ...state,
        loading: true,
      };

    case TYPE.GET_LIST_TABLE_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.data,
      };

    case TYPE.GET_LIST_TABLE_ERROR:
      return {
        ...state,
        loading: false,
        mess: action.mess,
      };

    default:
      return state;
  }
};

export default tableReducer;
