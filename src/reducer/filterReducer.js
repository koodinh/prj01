import * as TYPE from "../contans/filter";

const initialState = {
  loading: false,
  data: [],
  mess: null,
};

const filterReducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPE.GET_LIST_FILTER:
      return {
        ...state,
        loading: true,
      };

    case TYPE.GET_LIST_FILTER_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.data,
      };

    case TYPE.GET_LIST_FILTER_ERROR:
      return {
        ...state,
        loading: false,
        mess: action.mess,
      };

    default:
      return state;
  }
};

export default filterReducer;
