import * as TYPE from "../contans/cart";

const initialState = {
  loading: false,
  data: [],
  mess: null,
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPE.SAVE_CART:
      return {
        ...state,
        data: action.data,
        loading: true,
      };

    default:
      return state;
  }
};

export default cartReducer;
