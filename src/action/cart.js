import * as TYPE from "../contans/cart";

export const saveCart = (params) => {
  return {
    type: TYPE.SAVE_CART,
    ...params,
  };
};
